#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    int retval;
    char *hashed = md5(guess, strlen(guess));
    //printf("Hash: %s Hashed: %s Guess: %s\n", hash, hashed, guess);
    // Compare the two hashes
    if (strcmp(hash, hashed) == 0)
    {
        retval = 1;
    }
    else
    {
        retval = 0;
    }
    // Free any malloc'd memory
    free(hashed);
    return retval;
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    int lines = 50;
    char **words = malloc(lines * sizeof(char *));
    FILE *d = fopen(filename, "r");
    
    char line[1000];
    int count = 0;
    while (fgets(line, 1000, d) != NULL)
    {
        if (count == lines)
        {
            lines += 50;
            words = realloc(words, lines * sizeof(char *));
        }
        line[strlen(line)-1] = '\0';
        char *word = malloc(strlen(line) * sizeof(char) + 1);
        strcpy(word, line);
        //printf("%d word: %s", count, word);
        words[count] = word;
        count++;
    }
    //words[0] = "Test";
    return words;
}

int countlines(char *filename)
{
    FILE *f = fopen(filename, "r");
    char line[100];
    int count = 0;
    while (fgets(line, 100, f) != NULL)
    {
        count++;   
    }
    fclose(f);
    return count;
}

int main(int argc, char *argv[])
{
    //printf("argc: %d", argc);
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen = countlines(argv[2]);;
    char **dict = read_dictionary(argv[2], &dlen);
    //printf("dlen: %d", dlen);
    // Open the hash file for reading.
    FILE *hashf = fopen(argv[1], "r");
    //printf("%c", &dict);
    char line[100];
    // For each hash, try every entry in the dictionary.
    while (fgets(line, 100, hashf) != NULL)
    {
        //printf("%s", line);
        line[strlen(line)-1] = '\0';
        for (int i = 0; i < dlen; i++)
        {
            //printf("%s %s\n", line, dict[i]);
            
            if (tryguess(line, dict[i]) == 1)
            {
                printf("%s %s\n", line, dict[i]);
            }
        }
    }
}
